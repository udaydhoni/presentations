# Positioning Content.

There are various ways to position our content in css. Some of them are as following:
1. Using position property.
2. Using float.
3. Some block level properties like padding and margin.

## Using position property

The position property takes five values:
1. static
2. relative
3. absolute
4. fixed
5. sticky

### static

This is the default value of position property. Elements which have this property are not affected by offset properties like top,bottom e.t.c. 
 
### relative

An element with position value relative is positioned relative to its normal position. It removes the element from normal flow.

Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.

### absolute

An element with position: absolute will be positioned with respect to its nearest Non-static ancestor. The positioning of this element does not depend upon its siblings or the elements which are at the same level.

### fixed

Any HTML element with position: fixed property will be positioned relative to the viewport. An element with fixed positioning allows it to remain at the same position even we scroll the page. We can set the position of the element using the top, right, bottom, left.

### sticky

It is kind of combination of relative and fixed. Element with position sticky and played a role between fixed & relative based on the position where it is placed. If the element is placed at the middle of the document then when the user scrolls the document, the sticky element starts scrolling until it touches the top. When it touches the top, it will be fixed at that place in spite of further scrolling

## float

The float CSS property is used to position the elements to the left, right, of its container along with permitting the text and inline elements to wrap around it.

## Block level properties

Properties like margin and padding also are helpful in positioning content.

## Usage of display property

We can also modify natural flow of elements using display property setting to a block or inline-block.

